import tkinter as tk
from tkinter import filedialog


class Application(tk.Text):
    def __init__(self, parent):
        tk.Text.__init__(self, parent)
        tk.Text.grid(self)
        button = tk.Button(parent, text="Save", command=self.saveAs)
        button.grid()

        font = tk.Menubutton(parent, text="Font")
        font.grid()
        font.menu = tk.Menu(font, tearoff=0)
        font["menu"] = font.menu
        helvetica = tk.IntVar()
        courier = tk.IntVar()

        font.menu.add_checkbutton(label="Courier", variable=courier, command=self.FontCourier)
        font.menu.add_checkbutton(label="Helvetica", variable=helvetica, command=self.FontHelvetica)

    def FontHelvetica(self):
        self.config(font="Helvetica")

    def FontCourier(self):
        self.config(font="Courier")

    def saveAs(self):
        """Script for the save as button to work"""
        text = self.retrieve_input()
        saveLocation = filedialog.asksaveasfilename()
        file = open(saveLocation, "w+")
        file.write(text)
        file.close()

    def retrieve_input(self):
        return self.get("1.0", 'end-1c')


def main():
    root = tk.Tk()
    root.title("nakistext")
    mainApp = Application(root)
    mainApp.mainloop()


if __name__ == '__main__':
    main()
